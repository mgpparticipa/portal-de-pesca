<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Leer CSV</title>
        <link rel="stylesheet" href="style.css">

        <style>             
            
            *{
                padding: 0;
                margin: 0;
            }

            /* Estilos para los componentes de la nube de palabras */
            .wordCloudImg{
                width: 100%;
                height: 500px;
                opacity: 0.4;
                position: absolute;
            }
            
            #wordCloud{
                margin: 0px;
                padding: 0px;

                margin-bottom: 30px;
                width: 100%;
                height: 500px;
                /* background: linear-gradient(rgb(200, 40, 50), rgb(145,167,43)); bob marley*/
                /*background: linear-gradient(rgb(200, 170, 159), rgb(145,167,43));*/
                /* background-image: url(mar\ mdp.jpg) ; */
                /* background: url(mardelplata.jpg); */
                background-repeat: no-repeat;
                font-weight: bolder;
            }
            
            /* Estilos para tabla*/
            .table-container{
                width: max-content;
                margin: 10px 20px;
                margin: auto;
            }

            table{
                display: block;
                border: 2px rgb(0, 0, 0) solid;
                border-collapse: collapse;
                width: 100%;
                margin: auto;                
            }

            thead{
                border: 0px blueviolet solid;
                margin: 5px 5px;
                padding: 5px 5px;
            }

            tbody{
                border: 0px rgb(231, 14, 177) solid;
            }

            tr{
                border: 0px green solid;
                margin: 5px 5px;
                padding: 5px 5px;
            }

            th, td {
                margin: 15px 15px;
                padding: 15px 15px;
                text-align: center;
            }

            th, .encabezado{
                border: 2px black solid;
                background: rgb(20, 134, 228);
                color : white;                
                text-transform: uppercase;
                /* margin: 0px 10px; */
                text-underline-offset: 0;
            }

            td{
                /* border: 2px black solid; */                
                /*border-bottom: 2px black solid;
                border-right: 2px black solid;
                margin: 0px 10px; */
                min-width: max-content;
                max-width: 200px;
                padding: 10px 10px;
                border: 2px black solid;
            }

            a{
                font-size: 20px;
                display: block;
                text-align: center;
                width: 300px;
                margin: 20px auto;
            }

            
            

            /* div para dibujar el grafico de evolucion*/

            p{
                font-size: 24px;
            }

            .container{
                background: rgb(150, 224, 238);
                width: 90%;
            }

            #evolution{
                margin-top: 30px ;
                margin-bottom: 30px;
                width: 80%;
                height: 500px;
                right: 300px;
            }

            .info-moluscos{

                display: inline-block;
            }

            canvas{
                display: inline-block;
            }

            
            
            
            img{
                width: 100%;
                height: 200px;
                opacity: 0.4;
                position: absolute;
            }

           

            
            /* Desembarques históricos del puerto de Mar del Plata. */
            .container-desembarques{
                background: rgb(155, 278, 140);
                width: 90%;
            }
            
            #graficoBarras {
                margin-top: 30px ;
                margin-bottom: 30px;
                width: 70%;
                height: 450px;
                margin-bottom: 50px;
                display: inline-block;
                position: static;
            }
            
            aside{
                width: 25%;
                height: 100%;
                display: inline-block;
                background: blueviolet;
            }

            
            /* Grafico torta */
            .container-cake{
                background: rgb(255, 238, 220);
                width: 90%;
            }

            #cake {
                width: 100%;
                height: 500px;
                /*background: pink;*/
            }

            .am5-tooltip-container div{
                background: black;
            }
            
        </style>
    </head>
    <body>        
        <!-- importo toda la libreria necesaria para los graficos -->
        <script src="https://cdn.amcharts.com/lib/5/index.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/percent.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/wc.js"></script>
        
        <!-- Chart code -->
        <script>
            am5.ready(function() {
                // Create root element
                // https://www.amcharts.com/docs/v5/getting-started/#Root_element
                var root = am5.Root.new("wordCloud");
                // Set themes
                // https://www.amcharts.com/docs/v5/concepts/themes/
                root.setThemes([
                am5themes_Animated.new(root)
                ]);
                // Add series
                // https://www.amcharts.com/docs/v5/charts/word-cloud/
                var series = root.container.children.push(am5wc.WordCloud.new(root, {
                categoryField: "tag",
                valueField: "weight",
                maxFontSize: am5.percent(15)
                }));

                // Configure labels
                series.labels.template.setAll({
                fontFamily: "Courier New"
                });

                setInterval(function() {  
                am5.array.each(series.dataItems, function(dataItem) {
                    var value = Math.random() * 65;
                    value = value - Math.random() * value;
                    dataItem.set("value", value);
                    dataItem.set("valueWorking", value);
                })
                }, 5000)

                // Data from:
                // https://insights.stackoverflow.com/survey/2021#section-most-popular-technologies-programming-scripting-and-markup-languages
                series.data.setAll([
                { tag: "Mar del Plata", weight: 70.96 },
                { tag: "CAPTURAS MARITIMAS", weight: 56.07 },
                { tag: "PUERTOS", weight: 48.24 },
                { tag: "Ciudad", weight: 47.08 },
                { tag: "PECES", weight: 35.35 },
                { tag: "Divisas", weight: 33.91 },
                { tag: "Exportaciones", weight: 30.19 },
                { tag: "", weight: 27.86 },
                { tag: "", weight: 27.13 },
                { tag: "Langostinos", weight: 24.31 },
                { tag: "MAR ARGENTINO", weight: 21.98 },
                { tag: "PULPO", weight: 21.01 },
                { tag: "Escollera Norte", weight: 10.75 },
                { tag: "Camarón", weight: 9.55 },
                { tag: "LANCHAS PESQUERAS", weight: 8.32 },
                { tag: "BARCOS PESQUEROS", weight: 7.03 },
                { tag: "Pejerrey", weight: 6.75 },
                { tag: "Crustáceos", weight: 6.02 },
                { tag: "Merluza", weight: 5.61 },
                { tag: "Calamar", weight: 5.1 },
                { tag: "Escollera Sur", weight: 5.07 },
                { tag: "ANCHOITA", weight: 4.66 },
                { tag: "DESEMBARQUES", weight: 4.66 },
                { tag: "Importaciones", weight: 3.01 },
                { tag: "", weight: 2.8 },
                { tag: "Moluscos", weight: 2.6 },
                { tag: "CABALLA", weight: 2.46 },
                { tag: "Almejas", weight: 2.12 },
                { tag: "Jurel", weight: 2.1 },
                { tag: "Caracol", weight: 1.88 },
                { tag: "PESCA", weight: 1.74 },
                { tag: "Cangrejo", weight: 1.33 },
                { tag: "CORNALITO", weight: 1.29 },
                { tag: "Tiburones", weight: 0.97 },
                { tag: "Corvina blanca", weight: 0.79 },
                { tag: "Gatuzo", weight: 0.65 },
                { tag: "Besugo", weight: 0.56 },
                { tag: "Bagre", weight: 0.53 },
                ]);
            }); // end am5.ready()
        </script>

        <!-- HTML -->
        <div id="wordCloud">
            <img class="wordCloudImg" src="https://storage.mardelplata.gov.ar/index.php/s/CkWCH2nKpTLrdFX/download/mardelplata.jpg">
        </div>


        
        <?php
            /*
                Funciones necesarias para leer y procesar los archivos CSV
            */

            function checkDelimiter($archivo){
                $delimiters = [";" => 0, "," => 0, "\t" => 0, "|" => 0];
                $handle = fopen($archivo, "r");
                $firstLine = fgets($handle);
                fclose($handle); 
                foreach ($delimiters as $delimiter => &$count) {
                    $count = count(str_getcsv($firstLine, $delimiter));
                }
                return array_search(max($delimiters), $delimiters);
            }

            function csvToArray($archivo){
                $result = array();
                $csv = file($archivo);
                //print_r($csv);
                // creo una expresion regular para verificar que contenga
                // numero y letras en minusculas y/o mayusculas
                $buscar = "/[a-zA-Z0-9]/";                    
                foreach($csv as $line) {
                    /* con el if verifico que line (la linea que se lee del archivo csv)
                        haga match con mi expresion regular, 
                        si contiene solo algun delimitador como: , ; u otro 
                        no se agrega al arreglo result, solo arrega caracteres de 
                        importancia como letras y numeros 
                    */
                    if(preg_match($buscar, $line)){
                        $result[] =  str_getcsv($line, checkDelimiter($archivo));
                    }

                    //echo "<br><br>Line --> ".$line;
                    //var_dump($line);
                    //echo "<br><br>";
                    //$result[] =  str_getcsv($line, $this->checkDelimiter());
                }
                return $result;
            }
        ?>

        <?php
            /*
            _____________________________________________________________________________________________________
            |    Codigo para los graficos                                                                       |
            |___________________________________________________________________________________________________|
            */
            // Funcion para dinujar la tabla
            function drawTable($data){
                // Testing de para ver como se procesa
                // primer array (nombres de columnas)
                //print_r($data[0]);
                //echo "\nAccion ---> ".$data[0][0];
                //echo "\nCatidad ---> ".$data[0][1];
                //echo "<br> Se van a generar ". count($data[0]) . " columnas.";
                //echo "<br> Se van a generar ". count($data) . " filas en total incluyendo los encabezados. <br><br>";
                ?>
                <div class="table-container">
                    <table>
                        <thead>
                            <tr>
                            <?php foreach($data[0] as $d){ ?>
                                    <th class="encabezado"> <?php echo str_replace("_"," ", $d); ?>  </th>
                            <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <tr> 
                            <?php 
                                for($i=1; $i< count($data); $i++){
                                    for($j=0; $j< count($data[$i]); $j++){
                                        if(isset($data[$i])){
                            ?>
                                            <td> <?php echo $data[$i][$j]; ?></td> 
                            <?php 
                                        }
                                    }
                            ?>
                            </tr>
                        </tbody>
                        <?php   } ?>
                    </table>
                </div>
        <?php  } ?>

            <?php 
                function drawEvolution($data){
            ?>
                    <!-- Chart code -->
                    <script>
                        am5.ready(function(){
                            // Create root element
                            // https://www.amcharts.com/docs/v5/getting-started/#Root_element
                            var root = am5.Root.new("evolution");
                            // Set themes
                            // https://www.amcharts.com/docs/v5/concepts/themes/
                            root.setThemes([
                            am5themes_Animated.new(root)
                            ]);
                            // Create chart
                            // https://www.amcharts.com/docs/v5/charts/xy-chart/
                            var chart = root.container.children.push(am5xy.XYChart.new(root, {
                            panX: false,
                            panY: false,
                            wheelX: "panX",
                            wheelY: "zoomX",
                            layout: root.verticalLayout
                            }));
                            // Add legend
                            // https://www.amcharts.com/docs/v5/charts/xy-chart/legend-xy-series/
                            var legend = chart.children.push(
                            am5.Legend.new(root, {
                                centerX: am5.p50,
                                x: am5.p50
                            })
                            );

                            /* --------------------------------------------------------------------------------------------- */
                            //      Aqui comienzo mi codigo para usar la libreria
                            /* --------------------------------------------------------------------------------------------- */

                            // Recolecto todos los años primero
                            let años = [];
                            <?php foreach($data[1] as $año) { ?>
                                    años.push("<?php echo $año; ?>");
                            <?php } ?>
                            
                            // array principal
                            let descripcionesConValores = [];    
                            // creo array para insertar los datos y luego insertar este en descripcionesConValores
                            let newArray = []; 
                            // empiezo a por el indice 1, porque el indice 0 contiene los encabezados de la tabla
                            <?php for($i=2; $i<count($data); $i++) { 
                                    for($j=0; $j < count($data[$i]); $j++){ 
                            ?>
                                    newArray.push("<?= $data[$i][$j]; ?>");
                            <?php   } ?>
                                    descripcionesConValores.push(newArray); // guardo el array auxiliar en el principal
                                    newArray = []; // borro el contenido para reusarlo
                            <?php } ?>

                            // Recolecto todas las descripciones para luego ponerlas en el grafico
                            let descripciones = [];
                            for(d = 0 ; d< descripcionesConValores.length; d ++){
                            descripciones.push(descripcionesConValores[d][0]);
                            //console.log("\nDescarte " + descripcionesConValores[d].shift());
                            }
                                
                            // recorro la matriz formada de columna completa en columna completa
                            // y voy guardando la info en un objeto
                            function getArrayObject(){
                                let array = [];
                                let objeto = {};          
                                for(a=1; a < años.length; a ++){
                                    objeto["year"] = años[a];
                                    for(v=0; v< descripcionesConValores.length; v ++){
                                    // asigno las claves del objeto
                                    propiedad = descripcionesConValores[v][0];
                                    // asigno valores a cada clave generada
                                    objeto[propiedad] = parseInt(descripcionesConValores[v][a]); 
                                    }
                                    array.push(objeto); // guardo objeto en array
                                    objeto = new Object(); // reseteo objeto a 0 pero reusarlo
                                }          
                                return array; // retorno array creado
                            }
                            
                            // data era un array para la demo, ahora le 
                            // asigno el array creado para que la libreria trabaje
                            var data = getArrayObject(); 

                            // Create axes
                            // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
                            var xRenderer = am5xy.AxisRendererX.new(root, {
                            cellStartLocation: 0.1,
                            cellEndLocation: 0.9
                            })

                            var xAxis = chart.xAxes.push(am5xy.CategoryAxis.new(root, {
                            categoryField: "year",
                            renderer: xRenderer,
                            tooltip: am5.Tooltip.new(root, {})
                            }));

                            xRenderer.grid.template.setAll({
                            location: 1
                            })

                            xAxis.data.setAll(data);

                            var yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
                            renderer: am5xy.AxisRendererY.new(root, {
                                strokeOpacity: 0.1
                            })
                            }));

                            // Add series
                            // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
                            function makeSeries(name, fieldName) {
                            var series = chart.series.push(am5xy.ColumnSeries.new(root, {
                                name: name,
                                xAxis: xAxis,
                                yAxis: yAxis,
                                valueYField: fieldName,
                                categoryXField: "year"
                            }));

                            series.columns.template.setAll({
                            tooltipText: "{name}, {categoryX}:{valueY}",
                            width: am5.percent(90),
                            tooltipY: 0,
                            strokeOpacity: 0
                            });

                            series.data.setAll(data);

                            // Make stuff animate on load
                            // https://www.amcharts.com/docs/v5/concepts/animations/
                            series.appear();
                            series.bullets.push(function() {
                                return am5.Bullet.new(root, {
                                locationY: 0,
                                sprite: am5.Label.new(root, {
                                    text: "{valueY}",
                                    fill: root.interfaceColors.get("alternativeText"),
                                    centerY: 0,
                                    centerX: am5.p50,
                                    populateText: true
                                })
                                });
                            });
                            legend.data.push(series);
                            }    
                            
                            // asi creo mis descripciones para el grafico
                            for(dd = 0; dd < descripciones.length; dd ++){
                            descripcion = descripciones[dd]
                            makeSeries(descripcion, descripcion);
                            } 

                            // Make stuff animate on load
                            // https://www.amcharts.com/docs/v5/concepts/animations/
                            chart.appear(1000, 100);

                        }); // end am5.ready()
                    </script>
                
                <!-- HTML -->
                <div class="container">
                    <aside>
                        <!-- <img src="molusco.jpg"> -->
                        <p>
                            Desembarques de Capturas Marítimas Totales - Por Especie y Mes
                            Período: 01/01/2022 - 31/12/2022
                        </p>
                    </aside>
                    <div id="evolution"> </div>
                </div>
                <?php } ?>          

                <!-- Chart code -->
                <?php function drawColumns($data){ ?>
                        <script>
                            am5.ready(function() {

                                // Create root element
                                // https://www.amcharts.com/docs/v5/getting-started/#Root_element
                                var root = am5.Root.new("graficoBarras");


                                // Set themes
                                // https://www.amcharts.com/docs/v5/concepts/themes/
                                root.setThemes([
                                am5themes_Animated.new(root)
                                ]);


                                // Create chart
                                // https://www.amcharts.com/docs/v5/charts/xy-chart/
                                var chart = root.container.children.push(am5xy.XYChart.new(root, {
                                panX: true,
                                panY: true,
                                wheelX: "panX",
                                wheelY: "zoomX",
                                pinchZoomX: true
                                }));

                                // Add cursor
                                // https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
                                var cursor = chart.set("cursor", am5xy.XYCursor.new(root, {}));
                                cursor.lineY.set("visible", false);


                                // Create axes
                                // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
                                var xRenderer = am5xy.AxisRendererX.new(root, { minGridDistance: 30 });
                                xRenderer.labels.template.setAll({
                                rotation: -90,
                                centerY: am5.p50,
                                centerX: am5.p100,
                                paddingRight: 15
                                });

                                xRenderer.grid.template.setAll({
                                location: 1
                                })

                                var xAxis = chart.xAxes.push(am5xy.CategoryAxis.new(root, {
                                maxDeviation: 0.3,
                                categoryField: "descripcion",
                                renderer: xRenderer,
                                tooltip: am5.Tooltip.new(root, {})
                                }));

                                var yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
                                maxDeviation: 0.3,
                                renderer: am5xy.AxisRendererY.new(root, {
                                    strokeOpacity: 0.1
                                })
                                }));


                                // Create series
                                // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
                                var series = chart.series.push(am5xy.ColumnSeries.new(root, {
                                name: "Series 1",
                                xAxis: xAxis,
                                yAxis: yAxis,
                                valueYField: "valor",
                                sequencedInterpolation: true,
                                categoryXField: "descripcion",
                                tooltip: am5.Tooltip.new(root, {
                                    labelText: "{valueY}"
                                })
                                }));

                                series.columns.template.setAll({ cornerRadiusTL: 5, cornerRadiusTR: 5, strokeOpacity: 0 });
                                series.columns.template.adapters.add("fill", function(fill, target) {
                                return chart.get("colors").getIndex(series.columns.indexOf(target));
                                });

                                series.columns.template.adapters.add("stroke", function(stroke, target) {
                                return chart.get("colors").getIndex(series.columns.indexOf(target));
                                });

                                // Set data                               
                                let encabezados = [];

                                <?php
                                foreach($data[0] as $dato) {
                                ?>
                                    encabezado = "<?= $dato; ?>";
                                    //console.log("\nEl dato es un: " + typeof(encabezado));
                                    encabezados.push(encabezado);
                                <?php
                                }
                                ?>

                                let descripciones = [];

                                <?php 
                                // empiezo a por el indice 1, porque el indice 0 contiene los encabezados de la tabla
                                for($i=1; $i<count($data); $i++) {
                                ?>
                                    descripciones.push("<?php echo $data[$i][0]; ?>")
                                <?php
                                }
                                ?>
                                
                                //console.log(descripciones)

                                let valores = [];

                                <?php 
                                for($i=1; $i<count($data); $i++) { 
                                ?>
                                    valores.push("<?php echo $data[$i][1]; ?>")
                                <?php
                                }
                                ?>
                                
                                function getArrayObject(){
                                    let array = [];
                                    for(i=0; i< descripciones.length; i ++){
                                        if(descripciones[i] != "" && parseInt(valores[i])){
                                        let objecto = {
                                            descripcion: descripciones[i],
                                            valor: parseInt(valores[i])
                                        };
                                        array.push(objecto);
                                        }
                                    }
                                    return array;
                                }
                                
                                xAxis.data.setAll(getArrayObject());
                                series.data.setAll(getArrayObject());

                                // Make stuff animate on load
                                // https://www.amcharts.com/docs/v5/concepts/animations/
                                series.appear(1000);
                                chart.appear(1000, 100);

                            }); // end am5.ready()
                        </script>
                <!-- HTML -->
                <div class="container-desembarques">                    
                    <div id="graficoBarras"> </div>
                    <aside>
                        <h2> Desembarques históricos </h2>
                        <p> Del puerto de Mar del Plata. Evolución Años 2000 - 2022.</p>
                    </aside>
                </div>
                <?php } ?>                
        
            <?php
                function drawCake($data){
            ?>
                    <!-- Chart code -->
                    <script>
                    am5.ready(function() {

                    // Create root element
                    // https://www.amcharts.com/docs/v5/getting-started/#Root_element
                    var root = am5.Root.new("cake");

                    // Set themes
                    // https://www.amcharts.com/docs/v5/concepts/themes/
                    root.setThemes([
                        am5themes_Animated.new(root)
                    ]);

                    // Create chart
                    // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/
                    var chart = root.container.children.push(
                        am5percent.PieChart.new(root, {
                        endAngle: 270
                        })
                    );

                    // Create series
                    // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Series
                    var series = chart.series.push(
                        am5percent.PieSeries.new(root, {
                        valueField: "value",
                        categoryField: "category",
                        endAngle: 270
                        })
                    );

                    series.states.create("hidden", {
                        endAngle: -90
                    });

                    // Set data
                    let encabezados = [];

                    <?php 
                        foreach($data[0] as $dato) { 
                    ?>
                            encabezados.push("<?php echo $dato; ?>")
                    <?php
                        }
                    ?>

                    let descripciones = [];

                    <?php 
                        for($i=1; $i<count($data); $i++) { 
                    ?>
                            descripciones.push("<?php echo $data[$i][0]; ?>")
                    <?php
                        }
                    ?>

                    let valores = [];

                    <?php 
                        for($i=1; $i<count($data); $i++) { 
                    ?>
                            valores.push("<?php echo $data[$i][1]; ?>")
                    <?php
                        }
                    ?>

                        function getArrayObject(){
                            let array = [];
                            for(i=0; i< descripciones.length; i ++){
                                if(descripciones[i] != "" && parseInt(valores[i])){
                                let data = {
                                    category: descripciones[i],
                                    value: parseInt(valores[i])
                                };
                                array.push(data);
                                }
                            }
                            return array;
                        }

                    // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Setting_data

                    series.data.setAll(getArrayObject())

                    series.appear(1000, 100);

                    }); // end am5.ready()
                    </script>

                    <!-- HTML -->
                    <div class="container-cake">
                        <div id="cake"></div>
                        <div>
                            <aside>
                                <p>
                                    Desembarques Peces - Crustáceos - Moluscos. Puerto de Mar del Plata. Año 2022
                                    Peces: 274.071,57
                                    Crustáceos: 15.153,62
                                    Moluscos: 92.649,57
                                    Total: 381.874,76
                                </p>
                            </aside>
                        </div>
                    </div>
                    
            <?php
            }
            ?>

        <?php

            $archivo = "https://storage.mardelplata.gov.ar/index.php/s/oPn4J4oK8xbwyQ9/download/peces-crustaceos-moluscos2022.csv";
            $data = csvToArray($archivo);
            drawCake($data);

            $archivo = "https://storage.mardelplata.gov.ar/index.php/s/a37QWiXesaNdrt3/download/desembarques_maritimos.csv";
            $data = csvToArray($archivo);
            drawColumns($data);
            
            $archivo = "https://storage.mardelplata.gov.ar/index.php/s/QrTYMdFfQpNPEE6/download/Desembarques_moluscos_2022.csv";            
            $data = csvToArray($archivo);
            drawEvolution($data);

            $archivo = "https://storage.mardelplata.gov.ar/index.php/s/QrTYMdFfQpNPEE6/download/Desembarques_moluscos_2022.csv";            
            $data = csvToArray($archivo);
            //echo "<h1> Mariscos </h1>";
            //drawEvolution($data);

        ?>
    </body>
</html>